package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

// ProcessLine searches for old in line to replace it by new
// It returns fnd=true, if the pattern was found, res with the result string
// and occ with the number of occurrence of old
func ProcessLine(lin, old, nw string)(fnd bool, res string, occ int){
	oltl := strings.ToLower(old)
	nltl := strings.ToLower(nw)
	res = lin
	if strings.Contains(lin, old) || strings.Contains(lin, oltl){
		fnd = true
		occ += strings.Count(lin, old)
		occ += strings.Count(lin, oltl)
		res = strings.Replace(lin, old, nw, -1)
		res = strings.Replace(res, oltl, nltl, -1)
	}
	return fnd, res, occ
}

func FindReplaceFile(src, dst, old, new string) (occ int, lns []int, err error){
	sf, err := os.Open(src); if err != nil { return occ, lns, err}
	defer sf.Close()
	dstFile, err := os.Create(dst); if err != nil { return occ, lns, err}
	defer dstFile.Close()

	s := bufio.NewScanner(sf)
	w := bufio.NewWriter(dstFile)
	defer w.Flush()
	old = old + " "
	new = new + " "
	lnsIdx := 1
	for s.Scan() {
		f, r, o := ProcessLine(s.Text(), old, new)
		if f {
			occ += o
			lns = append(lns, lnsIdx)
		}
		fmt.Fprintf(w,r)
		lnsIdx++
	}
	return occ, lns, nil
}

func main(){
	o := "Go"
	n := "Javascript"
	occ, lines, err := FindReplaceFile("file.txt", "file_modified.txt",o,n)
	if err != nil { fmt.Printf("Erreur %v", err) }

	fmt.Println("=== Summary ===")
	defer fmt.Println("=== End of Summary ===")
	fmt.Printf("Numbers of \"%v\" occurrence : %v \n", o, occ)
	fmt.Printf("Numbers of lines: %d \n",len(lines))
	fmt.Print("Lines : [")
	len := len(lines)
	for i, l := range lines {
		fmt.Printf("%v", l)
		if i < len - 1 {
			fmt.Print(" - ")
		}
	}
	fmt.Println("]")
}